export interface Car {
    'id': number,
    'nombre': string,
    'ap_paterno': string,
    'ap_materno': string,
    'CURP': string,
    'RFC': string
}

