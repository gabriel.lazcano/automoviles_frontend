import { Car } from 'src/app/interfaces/Car';
import { CarsService } from './../../services/cars.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {
  public car: Car[];

  constructor(private carsService: CarsService) {
    this.car = [];
  }

  ngOnInit(): void {
    this.getCar();
  }

  getCar(){
    this.carsService.getCar().subscribe(res => {
      this.car = res;
      console.log(res);
    });
  }
  

}
