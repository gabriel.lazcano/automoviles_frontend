import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';


import { Car } from '../interfaces/Car';


@Injectable({
  providedIn: 'root'
})
export class CarsService {

  API_URL = 'https://comercializadoraautos-production.up.railway.app/api';

constructor(private http: HttpClient) { }

getCar(): Observable<Car[]>{
  return this.http.get<Car[]>(`${this.API_URL}/usuarios`).pipe(map((res: any)=> res.usuarios));
}

}
